﻿using UnityEngine;

[ExecuteInEditMode]
public class DepthCamera : MonoBehaviour
{

    private Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
        cam.depthTextureMode = DepthTextureMode.Depth;
    }

}