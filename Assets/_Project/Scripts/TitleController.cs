﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Touch touch;
		bool touched = !(Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began);
		if (touched)
		{
			SceneManager.LoadSceneAsync(1);
		}
	}
}
