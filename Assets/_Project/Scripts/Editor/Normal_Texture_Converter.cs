﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class NormalTextureTextureProcessor : AssetPostprocessor {
	void OnPostprocessTexture(Texture2D texture)
	{
		string lowerCaseAssetPath = assetPath.ToLower();

		if (lowerCaseAssetPath.IndexOf("_n.") >= 0 
		    || lowerCaseAssetPath.IndexOf("_normal.") >= 0
		    )
		{
			TextureImporter importer = assetImporter as TextureImporter;
			if (importer.textureType != TextureImporterType.NormalMap)
			{
				importer.textureType = TextureImporterType.NormalMap;
				importer.SaveAndReimport();
			}
		}
	}
}