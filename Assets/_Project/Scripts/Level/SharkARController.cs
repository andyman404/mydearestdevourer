﻿//using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using GoogleARCore.HelloAR;
using UnityEngine.PostProcessing;
using System.Collections;
using System.IO;

/// <summary>
/// Controls the SharkAR example.
/// </summary>
public class SharkARController : MonoBehaviour
{
    /// <summary>
    /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
    /// </summary>
    public Camera FirstPersonCamera;

    public Text debugText;

    public Transform water;
    private Anchor waterAnchor = null;

    private int stage = 0;

    /// <summary>
    /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
    /// </summary>
    private bool m_IsQuitting = false;

    /// <summary>
    /// A prefab for tracking and visualizing detected planes.
    /// </summary>
    public GameObject TrackedPlanePrefab;

    /// <summary>
    /// A gameobject parenting UI for displaying the "searching for planes" snackbar.
    /// </summary>
    public GameObject SearchingForPlaneUI;

    public Transform shark;
    public Animator sharkAnimator;
    public float sinkDown = 0.1f;

    public PostProcessingBehaviour postProcessingBehaviour;
    public PostProcessingProfile surfaceProfile;
    public PostProcessingProfile underwaterProfile;

    public void Start()
    {
        InitSharkAnimHashes();
    }

    /// <summary>
    /// The Unity Update() method.
    /// </summary>
    public void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        _QuitOnConnectionErrors();

        // Check that motion tracking is tracking.
        if (Frame.TrackingState != TrackingState.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
            return;
        }

        Screen.sleepTimeout = SleepTimeout.NeverSleep;


        Pose pose = Frame.Pose;

        if (shark.gameObject.activeSelf)
        {
           UpdateShark(pose);

        }

        if (water.gameObject.activeSelf)
        {
            postProcessingBehaviour.profile = (pose.position.y < water.position.y) ? underwaterProfile : surfaceProfile;
        }


		// If the player has not touched the screen, we are done with this update.
		Touch touch;
		bool touched = !(Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began);

		if (paused && touched)
		{
			Resume();
		}

		if (setWaterLevelQueued)
		{
			SetWaterLevel(pose);
			setWaterLevelQueued = false;
		}

		if (dropSwimmerQueued)
		{
			dropSwimmerQueued = false;
			DropSwimmer(pose);
		}

		if (takePictureQueued)
		{
			takePictureQueued = false;
			TakePicture();
		}

	}

	public void SetWaterLevel(Pose pose)
	{
		waterAnchor = Session.CreateWorldAnchor(pose);
		water.position = pose.position + Vector3.down * sinkDown;
		water.parent = waterAnchor.transform;
		water.gameObject.SetActive(true);
		shark.position = water.position + pose.rotation * Vector3.forward * 5.0f;
		shark.parent = water;
		shark.gameObject.SetActive(true);

		swimButton.interactable = true;
		photoButton.interactable = true;

		stage++;
	}

	public Transform swimmerPrefab;

	public void DropSwimmer(Pose pose)
	{
		Transform swimmer = Instantiate(swimmerPrefab, pose.position + pose.rotation * Vector3.forward, Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f)) as Transform;
		swimmer.parent = water;
	}

	public IEnumerator Pauser()
	{
		yield return new WaitForSecondsRealtime(0.1f);
		paused = true;
	}

	public void TakePicture()
	{
		// couldn't get screenshot sharing to work, so we're just pausing it

		guiCanvas.SetActive(false);
		
		Time.timeScale = 0.0f;

		StartCoroutine(Pauser());
	}


	public void Resume()
	{
		guiCanvas.SetActive(true);

		Time.timeScale = 1.0f;
		paused = false;

	}

	public GameObject guiCanvas;
	public bool paused = false;


	private float nextSharkDepthTime = 0.0f;
    private float targetDepth = 0.0f;
    private Vector3 targetOffset;
    public Rigidbody sharkRb;
    private bool isJump = false;
    private bool expireOnSubmerge = false;
    private bool leapOnJump = false;

	public Button sharkButton, swimButton, photoButton;

	public void QueueSetWaterLevel()
	{
		setWaterLevelQueued = true;
	}

	public void QueueDropSwimmer()
	{
		dropSwimmerQueued = true;
	}

	public void QueueTakePicture()
	{
		takePictureQueued = true;
	}
	public bool setWaterLevelQueued = false;
	public bool dropSwimmerQueued = false;
	public bool takePictureQueued = false;

    private void UpdateShark(Pose pose)
    {
        Vector3 poseForeward = pose.rotation * Vector3.forward;
        poseForeward.Normalize();
        poseForeward *= 5.0f;

        if (Time.time > nextSharkDepthTime)
        {
            nextSharkDepthTime = Time.time + Random.Range(3.0f, 6.0f);

			Swimmer firstSwimmer = water.GetComponentInChildren<Swimmer>();

			if (firstSwimmer != null && !firstSwimmer.bitten && Random.value < 0.25f)
			{
				isJump = true;
				targetDepth = 1f;
				targetOffset = firstSwimmer.transform.position - pose.position;
				targetOffset.y = targetDepth;
				leapOnJump = true;
				expireOnSubmerge = false;
			}
			else if (Random.value < 0.25f && targetDepth < -2.5f)
            {
                isJump = true;
                targetDepth = 10.0f;
                Vector2 circle = Random.insideUnitCircle.normalized * Random.Range(0.0f, 3.0f);

                targetOffset = poseForeward + new Vector3(targetOffset.x + circle.x, targetDepth, targetOffset.z + circle.y);
                leapOnJump = true;
                expireOnSubmerge = false;
            }
            else if (Random.value < 0.5f)
            {
                isJump = false;
                targetDepth = Random.Range(-1.0f, -0.3f);
                Vector2 circle = Random.insideUnitCircle.normalized * Random.Range(10.0f, 20.0f);

                targetOffset = poseForeward + new Vector3(circle.x, Mathf.Min(0.0f, targetDepth), circle.y);
                leapOnJump = false;
                expireOnSubmerge = false;

            }
            else
            {
                isJump = false;
                targetDepth = Random.Range(-8.0f, -1.0f);
                Vector2 circle = Random.insideUnitCircle.normalized * Random.Range(10.0f, 20.0f);

                targetOffset = poseForeward + new Vector3(circle.x, Mathf.Min(0.0f, targetDepth), circle.y);
                leapOnJump = false;
                expireOnSubmerge = false;
            }
        }

        Vector3 targetPos = pose.position + targetOffset;
        if (!isJump)
        {
            targetPos.y = water.position.y + Mathf.Min(0.0f, targetDepth);
        }
        else
        {
            targetPos.y = water.position.y + targetDepth;
        }
        
        Vector3 diff = targetPos - shark.position;
        float dist = diff.magnitude;
        Vector3 dir = diff.normalized;

        Quaternion targetRot = Quaternion.LookRotation(dir);
        Quaternion originalRot = sharkRb.rotation;

        float mouth = isJump ? 1.0f : 0.0f;
        float twistY = 0.0f;

        // airborne
        if (sharkRb.position.y > water.position.y)
        {
            Vector3 v = sharkRb.velocity;
            if (leapOnJump)
            {
                leapOnJump = false;
                v.y += Random.Range(10.0f, 15.0f);
            }
            v += Physics.gravity * 4.0f * Time.deltaTime;
            sharkRb.velocity = v;
            sharkRb.rotation = Quaternion.Slerp(sharkRb.rotation, Quaternion.LookRotation(v.normalized), 2.0f * Time.deltaTime);

            twistY = v.y > 0.0f ? 1.0f : -1.0f;
            expireOnSubmerge = true;
            //debugText.text = "Jump";
            mouth = 1.0f;

        }
        else
        {
            if (expireOnSubmerge)
            {
                //debugText.text = "Submerge";

                nextSharkDepthTime = Time.time + Random.Range(1.0f, 3.0f);

                isJump = true;
                targetDepth = Random.Range(-5.0f, -3.5f);
                Vector2 circle = Random.insideUnitCircle.normalized * Random.Range(10.0f, 20.0f);

                targetOffset = poseForeward + new Vector3(circle.x, Mathf.Min(0.0f, targetDepth), circle.y);
                leapOnJump = false;
                expireOnSubmerge = false;
                twistY = 0.0f;

                Vector3 v = sharkRb.velocity;
                v += Physics.gravity * 4.0f * Time.deltaTime;
                sharkRb.velocity = v;
                sharkRb.rotation = Quaternion.Slerp(sharkRb.rotation, Quaternion.LookRotation(v.normalized), 2.0f * Time.deltaTime);

            }
            else
            {
                //sharkRb.rotation = Quaternion.RotateTowards(sharkRb.rotation, targetRot, 60.0f * Time.deltaTime);
                float rotationLerpRate = isJump ? 4.0f : 1.0f;
                float newSpeed = isJump ? 8.0f : 4.0f;
                speed = Mathf.Lerp(speed, newSpeed, 2.0f * Time.deltaTime);

                sharkRb.rotation = Quaternion.Lerp(sharkRb.rotation, targetRot, rotationLerpRate * Time.deltaTime);
                sharkRb.velocity = sharkRb.rotation * Vector3.forward * speed;

                twistY = 0.0f;
            }
        }

        float twistXAngle = Vector3.SignedAngle(dir, shark.forward, Vector3.up);
        //debugText.text = twistXAngle.ToString();

        float twistX =
            Mathf.Sin(Time.timeSinceLevelLoad * Mathf.PI * tailFrequency) * 0.25f // base swimming
            - twistXAngle / 360.0f;

        if (Vector3.Distance(pose.position, sharkRb.position) < 5.0f)
        {
            mouth = 1.0f;
        }

        sharkAnimator.SetFloat(twistXHash, twistX);
        sharkAnimator.SetFloat(twistYHash, twistY, 0.5f, Time.deltaTime);

        sharkAnimator.SetFloat(mouthHash, mouth, 0.5f, Time.deltaTime);

    }
    private float speed;

    private int twistXHash;
    private int twistYHash;
    private int mouthHash;

    private void InitSharkAnimHashes()
    {
        twistXHash = Animator.StringToHash("twist.x");
        twistYHash = Animator.StringToHash("twist.y");
        mouthHash = Animator.StringToHash("mouth");
    }
    float tailFrequency = 2.0f;

    /// <summary>
    /// Quit the application if there was a connection error for the ARCore session.
    /// </summary>
    private void _QuitOnConnectionErrors()
    {
        if (m_IsQuitting)
        {
            return;
        }

        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.ConnectionState == SessionConnectionState.UserRejectedNeededPermission)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("DoQuit", 0.5f);
        }
        else if (Session.ConnectionState == SessionConnectionState.ConnectToServiceFailed)
        {
            _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("DoQuit", 0.5f);
        }
    }

    /// <summary>
    /// Actually quit the application.
    /// </summary>
    private void DoQuit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Show an Android toast message.
    /// </summary>
    /// <param name="message">Message string to show in the toast.</param>
    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }
}
