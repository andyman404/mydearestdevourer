﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swimmer : MonoBehaviour {

	public Rigidbody rb;
	public bool bitten = false;

	
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 pos = transform.position;
		Vector3 parentPos = transform.parent.position;

		if (bitten)
		{
			transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * 5.0f);
			if (Time.time > deadTime)
			{
				Destroy(gameObject);
				gameObject.SetActive(false);
			}
		}
		else
		{
			// falling
			if (pos.y > parentPos.y)
			{
				Vector3 v = rb.velocity;
				v += Physics.gravity * Time.deltaTime;
				rb.velocity = v;
			}
			else
			{
				Vector3 v = rb.velocity;
				v -= Physics.gravity * Time.deltaTime * 0.5f;
				rb.velocity = v;
			}

			Vector3 forward = transform.forward;
			forward.y = 0.0f;
			forward = Vector3.Lerp(forward, Random.insideUnitSphere, 0.1f * Time.deltaTime);
			forward.Normalize();

			rb.rotation = Quaternion.Slerp(rb.rotation, Quaternion.LookRotation(forward), 0.5f * Time.deltaTime);
		}

	}

	private float deadTime = 0.0f;

	public AudioClip screamSound;
	public GameObject particleSystemGO;

	public void OnCollisionStay(Collision collision)
	{
		if (bitten) return;

		int otherLayer = collision.gameObject.layer;

		if (otherLayer == 9)
		{
			Shark shark = collision.transform.GetComponent<Shark>();
			if (shark == null)
			{
				return;
			}

			AudioSource.PlayClipAtPoint(screamSound, transform.position, 1.0f);
			particleSystemGO.SetActive(true);
			bitten = true;
			rb.isKinematic = true;
			collision.collider.enabled = false;

			deadTime = Time.time + 3.0f;

			transform.parent = shark.mouth;
		}
	}
}
