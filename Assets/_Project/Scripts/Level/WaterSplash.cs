﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WaterSplash : MonoBehaviour {
    public ParticleSystem bigSplashParticleSystem;
    private Transform bigSplashTransform;
    public Text debugText;

	// Use this for initialization
	void Start () {
        bigSplashTransform = bigSplashParticleSystem.transform;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider collider)
    {
		bigSplashTransform.position = collider.transform.position;
        bigSplashParticleSystem.Emit(Mathf.CeilToInt(collider.attachedRigidbody.velocity.sqrMagnitude));
    }
}
