﻿Shader "Custom/Water2"
{
    Properties
    {
        _MainTex("Albedo", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)

        [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        _Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5

        _BumpScale("Scale", Float) = 1.0
        _BumpMap("Normal Map", 2D) = "bump" {}
		_BumpMoveSpeed("Bump Move Speed", Float) = 1.0

        _DistortionStrengthScaled ("DistortionStrengthScaled", Float) = 0.0
        _DistortionBlend("Blend", Range(0.0, 1.0)) = 0.5

		//_FoamDepth("Foam Depth", Float) = 0.1
		//_FoamColor("Foam Color", Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" "IgnoreProjector"="True" "PreviewType"="Plane" "PerformanceChecks"="False" }

		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

        GrabPass
        {
            Tags { "LightMode" = "Always" }
            "_GrabTexture"
        }

        Pass
        {
            Name "ShadowCaster"
            Tags { "LightMode" = "ShadowCaster" }

            BlendOp Add
            Blend One Zero
            ZWrite On
            Cull Off

            CGPROGRAM
            #pragma target 3.5
			#define _ALPHAPREMULTIPLY_ON 1
            #pragma multi_compile_shadowcaster

            #pragma vertex vertParticleShadowCaster
            #pragma fragment fragParticleShadowCaster

            #include "UnityStandardParticleShadow.cginc"
            ENDCG
        }

        CGPROGRAM
        #pragma surface surf Standard nolightmap nometa noforwardadd keepalpha vertex:vert
        #pragma target 3.5

		#define _ALPHAPREMULTIPLY_ON 1
		#define _NORMALMAP 1
		#define _DISTORTION_ON 1

		sampler2D _MainTex;
		float4 _MainTex_ST;
		half4 _Color;
		sampler2D _BumpMap;
		half _BumpScale;
		half _BumpMoveSpeed;
		half _Metallic;
		half _Glossiness;
		UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);
		sampler2D _GrabTexture;
		half _DistortionStrengthScaled;
		half _DistortionBlend;
		//half _FoamDepth;
		//half4 _FoamColor;

		// Vertex shader input
		struct appdata_particles
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 color : COLOR;
			float2 texcoords : TEXCOORD0;
			float4 tangent : TANGENT;
		};

		// Surface shader input
		struct Input
		{
			float4 color : COLOR;
			float2 texcoord;
		    //float4 projectedPosition;
			float4 grabPassPosition;
		};

		fixed4 readTexture(sampler2D tex, Input IN)
		{
			fixed4 color = tex2D (tex, IN.texcoord);
			return color;
		}

		#define vertTexcoord(v, o) \
			o.texcoord = TRANSFORM_TEX(v.texcoords.xy, _MainTex);

		#define vertDistortion(o) \
		    o.grabPassPosition = ComputeGrabScreenPos (clipPosition);

		#define vertFading(o) \
			o.projectedPosition = ComputeScreenPos (clipPosition); \
			COMPUTE_EYEDEPTH(o.projectedPosition.z);

		#define fragDistortion(i) \
			float4 grabPosUV = UNITY_PROJ_COORD(i.grabPassPosition); \
			grabPosUV.xy += normal.xy * _DistortionStrengthScaled * albedo.a; \
			half3 grabPass = tex2Dproj(_GrabTexture, grabPosUV).rgb; \
			albedo.rgb = lerp(grabPass, albedo.rgb, saturate(albedo.a - _DistortionBlend));

		void vert (inout appdata_particles v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float4 clipPosition = UnityObjectToClipPos(v.vertex);

			vertTexcoord(v, o);
			//vertFading(o);
			vertDistortion(o);
		}

		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			half4 albedo = readTexture (_MainTex, IN);
			albedo *= _Color;

			fixed2 metallicGloss = fixed2(_Metallic, _Glossiness);

			float2 uv1 = IN.texcoord + float2(_Time.y, _Time.y)*_BumpMoveSpeed;
			float2 uv2 = IN.texcoord * float2(1.1, -1.1) + float2(0.0, +_Time.y * _BumpMoveSpeed);
			float2 uv3 = IN.texcoord * float2(-1.1, 1.1) + float2(-_Time.y * _BumpMoveSpeed, 0.0);

			float3 normal1 = UnpackScaleNormal (tex2D (_BumpMap, uv1), _BumpScale);
			float3 normal2 = UnpackScaleNormal (tex2D (_BumpMap, uv2), _BumpScale);
			float3 normal3 = UnpackScaleNormal (tex2D (_BumpMap, uv3), _BumpScale);

			float3 normal = normalize((normal1+normal2+normal3)/3.0);

			half3 emission = 0;

			fragDistortion(IN);

			// couldn't get foam working in surface shader
	        //float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(IN.projectedPosition)));
			//half4 foamLine = 1.0 - saturate(_FoamDepth * (sceneZ - IN.projectedPosition.w)); // foam line by comparing depth and screenposition
			//albedo = lerp(albedo, _FoamColor, foamLine);
						
			o.Albedo = albedo.rgb;
			o.Normal = normal;
			o.Emission = emission;
			o.Metallic = metallicGloss.r;
			o.Smoothness = metallicGloss.g;

			o.Alpha = albedo.a;
		}
        ENDCG
    }

    Fallback "VertexLit"
    //CustomEditor "StandardParticlesShaderGUI"
}
